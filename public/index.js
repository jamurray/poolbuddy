/* globals $ io pb */

// ------ Start Socket Handling ------------------
var socket = io(`https://${window.location.host}`) // eslint-disable-line no-unused-vars

// ------- Load Scripts -------------------
$.when(
  $.getScript('poolbuddy/poolBuddy.js'),
  $.getScript('poolbuddy/f5Auth.js'),
  $.getScript('poolbuddy/f5DeviceSelection.js'),
  $.getScript('poolbuddy/f5PoolSelection.js'),
  $.getScript('poolbuddy/f5PoolMembersView.js'),
  $.getScript('poolbuddy/f5PoolProfileManager.js'),
  $.getScript('poolbuddy/windowsControl.js'),
  // $.getScript('poolbuddy/test.js'),
  $.Deferred(function (deferred) {
    $(deferred.resolve)
  })
).done(function () {
  // Start poolBuddy
  pb.start()
})

// ------------- TEST --------------
function sleep (ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

async function testScenario1 () {
  await sleep(1000)
  var username1 = 'ecajwm'
  var password1 = ``
  $('#f5UsernameField')[0].value = username1
  $('#f5PasswordField')[0].value = password1
}
// testScenario1()

async function testScenario2 () {
  await sleep(2000)
  pb.tokens = JSON.parse(window.sessionStorage.getItem('pb.tokens'))
  
  // Open graph.stage.amctheatres.com_pool
  pb.loadF5Selection()
  await sleep(1500)
  $('#f5SelectionDiv').find('li:contains("external-stage")').click()
  await sleep(1500)
  $('#f5PoolSelectionDiv').find('li:contains("graph.stage.amctheatres.com_pool")').click()

  await sleep(2500)
  // Shrink Columns
  colsToShrink = [1,3,4,5,7,8,9]
  colsToShrink.forEach(function (num) {
    pb.contextColumn = $('#f5PoolMembersDiv').children().children(`.cell:nth-child(${num})`)
    $(pb.contextColumn).toggleClass('shrunkColumn')
  })
  
  // Save auth tokens between refresh
  while (true) {
    await sleep(5000)
    window.sessionStorage.setItem('pb.tokens', JSON.stringify(pb.tokens))
  }
}
// testScenario2()
