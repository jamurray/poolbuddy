/* globals $ socket */

var pbF5DeviceSelection = function (pb) { // eslint-disable-line no-unused-vars
  // public vars
  pb.selectedF5Ip = ''
  pb.activeToken = ''
  pb.searchUrlNodes = ''
  pb.searchUrlNodeStats = ''
  pb.searchUrlPools = ''
  pb.f5Nodes = []
  pb.f5NodesStats = []
  pb.f5Pools = []
  pb.f5DeviceStates = []

  // private vars
  var f5SelectionDiv = `
    <div id="f5SelectionDiv">
      <ul id="f5SelectionUl">
      </ul>
    </div>
  `
  // functions
  pb.loadF5Selection = function () {
    $('#f5AuthDiv, #f5SelectionDiv').remove()
    $('body').append(f5SelectionDiv)
    // Add each F5 to the list
    pb.clientConfig.f5List.forEach(function (f5) {
      let li = document.createElement('li')
      li.innerHTML = `<span>${f5.nickname}</span><span class="failoverState"></span>`
      $(li).data(f5)
      $('#f5SelectionUl').append(li)
    })
    // Add click events to li
    $('#f5SelectionUl').find('li').click(function () {
      if ($(this).hasClass('selected')) {
        console.log('F5 already selected')
      } else {
        $('#f5SelectionDiv').siblings().remove()
        $(this).addClass('selected').siblings().removeClass('selected')
        pb.selectedF5Ip = $(this).data().ip
        let tokenIndex = pb.tokens.findIndex(function (t) { return t.ip === pb.selectedF5Ip })
        pb.activeToken = pb.tokens[tokenIndex].token

        // Call for Nodes/NodesStats/Pools
        pb.searchUrlNodes = `https://${pb.selectedF5Ip}/mgmt/tm/ltm/node/`
        pb.searchUrlNodeStats = `https://${pb.selectedF5Ip}/mgmt/tm/ltm/node/stats`
        pb.searchUrlPools = `https://${pb.selectedF5Ip}/mgmt/tm/ltm/pool/`
        pb.getF5Nodes()
        pb.getF5NodesStats()
        pb.getF5Pools()
      }
    })
    pb.getF5DeviceStates()
  }
  pb.getF5Nodes = function () {
    socket.emit('getF5Nodes', {
      token: pb.activeToken,
      url: pb.searchUrlNodes
    })
  }
  pb.getF5NodesStats = function () {
    socket.emit('getF5NodesStats', {
      token: pb.activeToken,
      url: pb.searchUrlNodeStats
    })
  }
  pb.getF5Pools = function () {
    socket.emit('getF5Pools', {
      token: pb.activeToken,
      url: pb.searchUrlPools
    })
  }

  pb.handleF5NodesResponse = function (data) {
    let promise = new Promise(async function (resolve, reject) {
      if (data.url === pb.searchUrlNodes) {
        pb.f5Nodes = data.items
        resolve(true)
      } else { resolve(false) }
    })
    return promise
  }

  pb.handleF5NodesStatsResponse = function (data) {
    let promise = new Promise(async function (resolve, reject) {
      if (data.url === pb.searchUrlNodeStats) {
        // Process Node Stats data into a useable array
        // The F5 api returns shitty nest of a JSON object for stats
        pb.f5NodesStats = []
        for (let prop in data.entries) {
          let nodeStats = {}
          let nestedStatsEntries = data.entries[prop].nestedStats.entries
          for (let p in nestedStatsEntries) {
            let stat = nestedStatsEntries[p].value || nestedStatsEntries[p].description || 0
            nodeStats[p] = stat
          }
          pb.f5NodesStats.push(nodeStats)
        }
        resolve(true)
      } else { resolve(false) }
    })
    return promise
  }
  
  pb.handleF5PoolsResponse = function (data) {
    let promise = new Promise(async function (resolve, reject) {
      if (data.url === pb.searchUrlPools) {
        pb.f5Pools = data.items
        resolve(true)
      } else { resolve(false) }
    })
    return promise
  }

  pb.getF5DeviceStates = function () {
    pb.tokens.forEach(function (t) {
      socket.emit('getF5DeviceStates', {
        token: t.token,
        url: `https://${t.ip}/mgmt/tm/cm/device`
      })
    })
  }
  pb.handleF5DeviceStates = function (data) {
    if (pb.tokens.length === pb.clientConfig.f5List.length) {
      data.items.forEach(function (device) {
        // Only 2 values we care about, but there is much more that may be useful later
        let ip = device.managementIp
        let failoverState = device.failoverState

        // Update pb.f5DeviceStates
        let index = pb.f5DeviceStates.findIndex(function (ds) {
          return ds.ip === ip 
        })
        if (index > -1) {
          pb.f5DeviceStates[index].failoverState = failoverState
        } else if (index === -1) {
          pb.f5DeviceStates.push({
            "ip": ip,
            "failoverState": failoverState
          })
        }

        // Update List Items
        let listItems = $('#f5SelectionUl').find('li')
        let matchingListItem = listItems.filter(function () {
          return $(this).data().ip === ip
        })[0]
        $(matchingListItem).data().failoverState = failoverState
        let failoverStateSpan = $(matchingListItem).find('span.failoverState')
        if (failoverState === "active") {
          $(failoverStateSpan).addClass('failoverActive').html(` Active`).attr("title", 'Failover State: Active')
        } else if (failoverState === "standby") {
          $(failoverStateSpan).addClass('failoverStandby').html(` Standby`).attr("title", 'Failover State: Standby')
        }
      })

      // Adjust List Item Width
      let widestWidth = 0
      $('#f5SelectionUl').find('li').find('span:first-child').each(function () {
        let width = $(this).width()
        if (width > widestWidth) { widestWidth = width}
      })
      $('#f5SelectionUl').find('li').find('span:first-child').width(widestWidth)      
    }
  }
  
  return pb
}
