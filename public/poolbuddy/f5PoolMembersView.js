var pbF5PoolMembersView = function (pb) { // eslint-disable-line no-unused-vars
  // public vars
  pb.contextColumn = {}
  pb.shrunkColumns = []

  // functions
  pb.loadF5PoolMembersView = function () {
    let promise = new Promise(async function (resolve, reject) {
      // Make Div
      $('#f5PoolMembersDiv').remove()
      let f5PoolMembersDiv = document.createElement('div')
      f5PoolMembersDiv.id = 'f5PoolMembersDiv'
      $(f5PoolMembersDiv).addClass('table')
      $('body').append(f5PoolMembersDiv)

      // Header Row
      let headerRow = document.createElement('div')
      $(headerRow).addClass('heading')
      headerRow.innerHTML = `
        <div class="cell dns">#</div>
        <div class="cell dns">Hostname</div>
        
        <div class="cell member">MemberName</div>
        <div class="cell member">Availability</div>
        <div class="cell member">Conns</div>
        <div class="cell member">State</div>
        
        <div class="cell node">ParentNode</div>
        <div class="cell node">Availability</div>
        <div class="cell node">Conns</div>
        <div class="cell node">State</div>
      `
      $('#f5PoolMembersDiv').append(headerRow)

      // Member Rows
      pb.f5PoolMembers.forEach(function (poolMember) {
        let rowDiv = document.createElement('div')
        $(rowDiv).addClass('row').data(poolMember)
        rowDiv.innerHTML = `
          <div class="cell dns">${poolMember.SUFFIXNUMBER}</div>
          <div class="cell dns">${poolMember.HOSTNAME}</div>
          
          <div class="cell member">${poolMember.name}</div>
          <div class="cell member"></div>
          <div class="cell member"></div>
          <div class="cell member">
            <button title="Enable Pool Member" class="stateButton enable member">E</button><!--
            --><button title="Disable Pool Member" class="stateButton disable member">D</button><!--
            --><button title="Force Pool Member Offline" class="stateButton force member">F</button>
          </div>
          
          <div class="cell node"></div>
          <div class="cell node"></div>
          <div class="cell node"></div>
          <div class="cell node">
            <button title="Enable Parent Node" class="stateButton enable node">E</button><!--
            --><button title="Disable Parent Node" class="stateButton disable node">D</button><!--
            --><button title="Force Parent Node Offline" class="stateButton force node">F</button>
          </div>
        `
        $('#f5PoolMembersDiv').append(rowDiv)
      })

      // Pool Member State Buttons Click Functions
      $('.stateButton.enable.member').click(function () {
        $(this).addClass('clicked').siblings().removeClass('clicked')
        let data = $(this).parents('.row').data()
        pb.changeStateF5PoolMember(data.selfLink, 'enable')
      })
      $('.stateButton.disable.member').click(function () {
        $(this).addClass('clicked').siblings().removeClass('clicked')
        let data = $(this).parents('.row').data()
        pb.changeStateF5PoolMember(data.selfLink, 'disable')
      })
      $('.stateButton.force.member').click(function () {
        $(this).addClass('clicked').siblings().removeClass('clicked')
        let data = $(this).parents('.row').data()
        pb.changeStateF5PoolMember(data.selfLink, 'force')
      })
      // Parent Node State Buttons
      $('.stateButton.enable.node').click(function () {
        $(this).addClass('clicked').siblings().removeClass('clicked')
        let data = $(this).parents('.row').data()
        pb.changeStateF5Node(data.PARENTNODE.selfLink, 'enable')
      })
      $('.stateButton.disable.node').click(function () {
        $(this).addClass('clicked').siblings().removeClass('clicked')
        let data = $(this).parents('.row').data()
        pb.changeStateF5Node(data.PARENTNODE.selfLink, 'disable')
      })
      $('.stateButton.force.node').click(function () {
        $(this).addClass('clicked').siblings().removeClass('clicked')
        let data = $(this).parents('.row').data()
        pb.changeStateF5Node(data.PARENTNODE.selfLink, 'force')
      })
      // await sleep(2000)
      resolve(true)
    })
    pb.shrinkColumns()
    return promise
  }

  // Member Column Stats
  pb.f5MembersStatsToRowDATA = function () {
    let promise = new Promise(async function (resolve, reject) {
      if ($('#f5PoolMembersDiv').length === 1) {
        let rows = $('#f5PoolMembersDiv').find('div.row')
        for (let i = 0; i < rows.length; i++) {
          let ip = $(rows[i]).data().address
          let mStatIndex = pb.f5PoolMembersStats.findIndex(function (memberStats) {
            return memberStats.addr === ip
          })
          let memberStats = pb.f5PoolMembersStats[mStatIndex]
          $(rows[i]).data().MEMBERSTATS = memberStats
        }
      }
      resolve(true)
    })
    return promise
  }
  pb.updateMemberStatsColumns = function () {
    try {
      let rows = $('#f5PoolMembersDiv').find('div.row')
      for (let i = 0; i < rows.length; i++) {
        let row = $(rows[i])
        let memberAvailabilityState = row.data().MEMBERSTATS['status.availabilityState']
        let memberEnabledState = row.data().MEMBERSTATS['status.enabledState']
        let memberStatusReason = row.data().MEMBERSTATS['status.statusReason']
        let memberConns = row.data().MEMBERSTATS['serverside.curConns'] || 0
        let cells = $(row).find('.cell')
        cells[3].innerHTML = `${memberAvailabilityState} (${memberEnabledState})`
        cells[4].innerHTML = memberConns
        // Member State Buttons
        let mState = ''
        if (memberStatusReason === 'Forced down') {
          mState = 'forced'
        } else if (memberEnabledState === 'disabled-by-parent') {
          mState = 'disabled'
        } else if (memberEnabledState === 'disabled') {
          mState = 'disabled'
        } else if (memberEnabledState === 'enabled') {
          mState = 'enabled'
        }
        let stateButtons = $(cells[5]).find('button')
        $(stateButtons).removeClass('curState')
        switch (mState) {
          case 'enabled':
            $(stateButtons[0]).addClass('curState').removeClass('clicked')
            break
          case 'disabled':
            $(stateButtons[1]).addClass('curState').removeClass('clicked')
            if (memberEnabledState === 'disabled-by-parent') { $(stateButtons[0]).removeClass('clicked') }
            break
          case 'forced':
            $(stateButtons[2]).addClass('curState').removeClass('clicked')
            break
        }
      }
    } catch (e) {
      console.log(e)
      console.log('member stats were probably being loaded to rows')
    }
  }

  // Parent Node Stats
  pb.f5NodesStatsToRowDATA = function () {
    let promise = new Promise(async function (resolve, reject) {
      if ($('#f5PoolMembersDiv').length === 1) {
        let rows = $('#f5PoolMembersDiv').find('div.row')
        for (let i = 0; i < rows.length; i++) {
          let ip = $(rows[i]).data().address
          let index = pb.f5NodesStats.findIndex(function (nodeStats) {
            return nodeStats.addr === ip
          })
          let parentNodeStats = pb.f5NodesStats[index]
          $(rows[i]).data().NODESTATS = parentNodeStats
        }
        resolve(true)
      } else { resolve(false) }
    })
    return promise
  }
  pb.updateNodeStatsColumns = function () {
    let rows = $('#f5PoolMembersDiv').find('div.row')
    for (let i = 0; i < rows.length; i++) {
      let row = $(rows[i])
      let nodeName = row.data().NODESTATS['tmName'].split('/')[row.data().NODESTATS['tmName'].split('/').length - 1]
      let nodeAvailabilityState = row.data().NODESTATS['status.availabilityState']
      let nodeEnabledState = row.data().NODESTATS['status.enabledState']
      let nodeStatusReason = row.data().NODESTATS['status.statusReason']
      let nodeConns = row.data().NODESTATS['serverside.curConns'] || 0
      let cells = $(row).find('.cell')
      cells[6].innerHTML = nodeName
      cells[7].innerHTML = `${nodeAvailabilityState} (${nodeEnabledState})`
      cells[8].innerHTML = nodeConns
      // Node State Buttons
      let nState = ''
      if (nodeStatusReason === 'Forced down') {
        nState = 'forced'
      } else if (nodeEnabledState === 'disabled-by-parent') {
        nState = 'disabled'
      } else if (nodeEnabledState === 'disabled') {
        nState = 'disabled'
      } else if (nodeEnabledState === 'enabled') {
        nState = 'enabled'
      }
      let stateButtons = $(cells[9]).find('button')
      $(stateButtons).removeClass('curState')
      switch (nState) {
        case 'enabled':
          $(stateButtons[0]).addClass('curState').removeClass('clicked')
          break
        case 'disabled':
          $(stateButtons[1]).addClass('curState').removeClass('clicked')
          break
        case 'forced':
          $(stateButtons[2]).addClass('curState').removeClass('clicked')
          break
      }
    }
  }

  // Parent Nodes (nothing really done with this data after added to row, but it is live and updated)
  pb.addF5NodesToMemberViewRowDATA = function () {
    let promise = new Promise(async function (resolve, reject) {
      if ($('#f5PoolMembersDiv').length === 1) {
        let rows = $('#f5PoolMembersDiv').find('div.row')
        for (let i = 0; i < rows.length; i++) {
          let ip = $(rows[i]).data().address
          let index = pb.f5Nodes.findIndex(function (parentNode) {
            return parentNode.address === ip
          })
          let parentNodeInfo = pb.f5Nodes[index]
          $(rows[i]).data().PARENTNODE = parentNodeInfo
        }
        resolve(true)
      } else { resolve(false) }
    })
    return promise
  }

  // Requesting State Changes
  pb.changeStateF5PoolMember = function (url, state) {
    url = url.replace('localhost', pb.selectedF5Ip).split('?')[0]
    socket.emit('changeStateF5PoolMember', {
      token: pb.activeToken,
      url: url,
      state: state
    })
  }
  pb.changeStateF5Node = function (url, state) {
    url = url.replace('localhost', pb.selectedF5Ip).split('?')[0]
    socket.emit('changeStateF5Node', {
      token: pb.activeToken,
      url: url,
      state: state
    })
  }

  // Column Context Menu
  pb.addColumnContextMenu = function () {
    if ($('#f5PoolMembersDiv').children(':first-child').length !== 1) {
      console.log('addColumnContextMenu, f5PoolMembersDiv first row not loaded yet')
    }
    $('.context-menu').remove()
    // Header Row Column Control
    // add a hidden Context Menu
    var contextMenu = `
      <div class='context-menu'>
        <ul>
          <li id="shrinkColumn">Shrink/Expand Column</li>
          
        </ul>
      </div>
    `
    $('body').find('#f5PoolMembersDiv').append(contextMenu)

    let hRow = $('#f5PoolMembersDiv').children(':first-child')
    let headerCells = $(hRow).find('.cell')
    for (let i = 0; i < headerCells.length; i++) {
      let headerCell = $(headerCells[i])
      
      headerCell.off('contextmenu')
      headerCell.on('contextmenu', function (e) {
        // console.log(e)
        var index = $(this).index()
        pb.contextColumn = $('#f5PoolMembersDiv').children().children(`.cell:nth-child(${index + 1})`) // jquery offset, 1 is first
        let top = e.pageY + 5
        let left = e.pageX
        $('.context-menu').toggle(100).css({
          top: top + 'px',
          left: left + 'px'
        })
        return false
      })
    }
    // Hide it when clicked outside of it
    $(document).bind('contextmenu click', function () {
      $('.context-menu').hide()
    })
    $('#shrinkColumn').click(function () {
      $(pb.contextColumn).toggleClass('shrunkColumn')
      let headerCells = $('#f5PoolMembersDiv').children(':first-child').find('.cell')
      let shrunkColumns = []
      for (let i = 0; i < headerCells.length; i++) {
        let cell = $(headerCells[i])
        shrunkColumns.push($(cell).hasClass('shrunkColumn'))
      }
      pb.shrunkColumns = shrunkColumns
    })
  }

  pb.shrinkColumns = function () {
    for (let i = 0; i < pb.shrunkColumns.length; i++) {
      if (pb.shrunkColumns[i]) {
        pb.contextColumn = $('#f5PoolMembersDiv').children().children(`.cell:nth-child(${i+1})`).addClass('shrunkColumn')
      } else {
        pb.contextColumn = $('#f5PoolMembersDiv').children().children(`.cell:nth-child(${i+1})`).removeClass('shrunkColumn')
      }
    }
  }

  return pb
}
