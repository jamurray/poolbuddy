// Add Columns to f5PoolMembersViews for"
// Ping
// Pending Updates/RestartRequired
// Run Updates
// Reboot
// Restart IIS

var pbWindowsControl = function (pb) {
  // public vars

  // private vars
  var headerCells = `
    <div class="cell windows">Ping</div>
    <div class="cell windows">UpdateState</div>
    <div class="cell windows">RestartIIS</div>
    <div class="cell windows">Reboot</div>
    <div class="cell windows">Task</div>
  `
  var rowCells = `
    <div class="cell windows">
      <span class="serverPingDot serverPingNeutral"></span>
    </div>
    <div class="cell windows"></div>
    <div class="cell windows">
      <button>RestartIIS</button>
    </div>
    <div class="cell windows">
      <button>Reboot</button>
    </div>
    <div class="cell windows taskDiv"></div>
  `

  // functions
  pb.addWindowsColumns = function () {
    let headerRow = $('#f5PoolMembersDiv').find('div.heading')
    $(headerRow).append(headerCells)
    
    let rows = $('#f5PoolMembersDiv').find('.row')
    for (let i = 0; i < rows.length; i++) {
      let row = $(rows[i])
      $(row).append(rowCells)
      if ($(row).data().HOSTNAME === '') {
        $(row).find('div.windows').find('button').remove()
      }
      // Add a click event to Reboot Button
      let rebootButton = $(row).find('button:contains("Reboot")')
      $(rebootButton).click(function () {
        let serverName = $(this).parents('.row').data().HOSTNAME
        let data = {
          serverName: serverName
        }
        socket.emit('rebootServer', data)
      })

      // Add a click event to the RestartIIS button
      let restartIISButton = $(row).find('button:contains("RestartIIS")')
      $(restartIISButton).click(function () {
        let serverName = $(this).parents('.row').data().HOSTNAME
        let data = {
          serverName: serverName
        }
        socket.emit('restartIIS', data)
      })
    }
    pb.addColumnContextMenu()
    pb.pingServers()
    pb.shrinkColumns()
  }

  pb.pingServers = async function () {
    let rows = $('#f5PoolMembersDiv').find('.row')
    for ( let i = 0; i < rows.length; i++) {
      let ip = $(rows[i]).data().address
      socket.emit('serverPing', {host: ip})
      await sleep(100)
    }
  }
  pb.continuallyPingServers = async function (ms) {
    while (true) {
      await sleep(ms)
      pb.pingServers()
    }
  }
  pb.handleServerPing = function (data) {
    try {
      let rows = $('#f5PoolMembersDiv').find('.row')
      if (rows.length > 0) {
        let matchingRow = rows.filter(function () {
          return $(this).data().address === data.requestedHost
        })[0]
        $(matchingRow).data().PING = data
        let pingDot = $(matchingRow).find('.serverPingDot')
        $(pingDot).prop('title', data.output)
        if (data.alive) {
          $(pingDot).removeClass('serverPingNeutral serverPingBad').addClass('serverPingGood')
        } else {
          $(pingDot).removeClass('serverPingNeutral serverPingGood').addClass('serverPingBad')
        }
      }
    } catch (err) {}
  }

  pb.getUpdateStates = function () {
    // Verify that ping has finished on all rows by checking the last row
    let ready = false
    try {
      if (typeof $('#f5PoolMembersDiv').find('.row').last().data().PING !== 'undefined') {
        ready = true
      }
    } catch (e) {}
    if (ready) {
      try {
        let pingableServerRows = $('#f5PoolMembersDiv').find('.row').filter(function () {
          return $(this).data().PING.alive
        })
        let serverNames = []
        for (let i = 0; i < pingableServerRows.length; i++) {
          let serverName = $(pingableServerRows[i]).data().HOSTNAME || ''
          if (serverName) {
            serverNames.push(serverName)
            if ($($(pingableServerRows[i]).find('div.cell.windows')[1]).text() === '') {
              $($(pingableServerRows[i]).find('div.cell.windows')[1]).text('...')
            }
          }
        }
        if (serverNames.length > 0) {
          let data = {
            // f5: pb.selectedF5Ip,
            // pool: pb.selectedPool,
            serverNames: serverNames
          }
          socket.emit('GetUpdateStatesForServers', data)
        }
      } catch (e) {
        console.error(e)
      }
    } else {
      // console.log('pings not completed')
    }
  }


  pb.continuallyGetUpdateStates = async function (ms) {
    while (true) {
      await sleep(ms)
      pb.getUpdateStates()
    }
  }

  pb.handleRebootServer = function (data) {
    try {
      let rows = $('#f5PoolMembersDiv').find('.row')
      let matchingRow = rows.filter(function () {
        return $(this).data().HOSTNAME === data.serverName
      })[0]
      let taskDiv = $(matchingRow).find('.taskDiv')[0]
      if (data.success) {
        let taskUri = `${data.octopusUrl}${data.octoResponse.Links.Web}`
        let link = `<a href="${taskUri}" target="_blank">RebootServer: ${data.octoResponse.Id}</a>`
        $(taskDiv).html(link)
      } else {
        $(taskDiv).html(`Reboot: ${data.reason}`)
      }
    } catch (e) {
      console.error(e)
    }
  }

  pb.handleRestartIIS = function (data) {
    try {
      let rows = $('#f5PoolMembersDiv').find('.row')
      let matchingRow = rows.filter(function () {
        return $(this).data().HOSTNAME === data.serverName
      })[0]
      let taskDiv = $(matchingRow).find('.taskDiv')[0]
      if (data.success) {
        let taskUri = `${data.octopusUrl}${data.octoResponse.Links.Web}`
        let link = `<a href="${taskUri}" target="_blank">RestartIIS: ${data.octoResponse.Id}</a>`
        $(taskDiv).html(link)
      } else {
        $(taskDiv).html(`RestartIIS: ${data.reason}`)
      }
    } catch (e) {
      console.error(e)
    }
  }



  return pb
}