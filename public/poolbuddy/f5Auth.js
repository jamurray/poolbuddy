/* globals $ socket */

// auth module as module augmentation for poolBuddy
var pbF5Auth = function (pb) { // eslint-disable-line no-unused-vars
  // public vars
  pb.tokens = []

  // private vars
  var failedTokens = []
  var remainingTokenCalls = []
  var f5AuthDiv = `
    <div id="f5AuthDiv">
      <img id="f5AuthImg" src="poolbuddy/images/sharkInPool.jpg">
      </br>
      <input type="text" id="f5UsernameField" placeholder="ecaUsername">
      <input type="password" id="f5PasswordField" placeholder="ecaPassword">
      <button type="button" id="f5SubmitCredsButton">submit</button>
      </br>
      <pre id="f5AuthText" class="authTextNeutral">poolBuddy</pre>
    </div>
  `
  // functions
  pb.addAuthDiv = function () {
    $('body').append(f5AuthDiv)
    $('#f5SubmitCredsButton').click(function () { pb.getAuthTokens() })
    $('#f5UsernameField').on('keyup', function (e) {
      if (e.which === 13) { $('#f5SubmitCredsButton').click() }
    })
    $('#f5PasswordField').on('keyup', function (e) {
      if (e.which === 13) { $('#f5SubmitCredsButton').click() }
    })
  }
  pb.getAuthTokens = async function () {
    pb.tokens = []
    failedTokens = []
    $('#f5AuthText').removeClass('authTextGood authTextBad').addClass('authTextNeutral')
    $('#f5SubmitCredsButton, #f5UsernameField, #f5PasswordField').prop('disabled', 'true')
    let username = $('#f5UsernameField')[0].value
    let password = $('#f5PasswordField')[0].value
    for (let i = 0; i < pb.clientConfig.f5List.length; i++) {
      let f5ip = pb.clientConfig.f5List[i].ip
      socket.emit('getF5AuthToken', {
        f5ip: f5ip,
        username: username,
        password: password
      })
      remainingTokenCalls.push(f5ip)
    }
  }
  pb.handleAuthTokenResponse = function (data, cb) {
    if (typeof data.token === "object") {
      pb.tokens.push({
        ip: data.f5ip,
        token: data.token.token,
        timeout: data.token.timeout,
        expiration: data.token.expirationMicros
      })
    } else {
      failedTokens.push(data)
    }
    let index = remainingTokenCalls.indexOf(data.ip)
    remainingTokenCalls.splice(index, 1)
    let ratioText = `(${pb.clientConfig.f5List.length - remainingTokenCalls.length}/${pb.clientConfig.f5List.length}) Getting F5 Auth Tokens...`
    $('#f5AuthText').text(ratioText)
    if (remainingTokenCalls.length === 0) {
      $('#f5SubmitCredsButton, #f5UsernameField, #f5PasswordField').removeAttr('disabled')
      if (failedTokens.length > 0) {
        $('#f5AuthText').removeClass('authTextNeutral').addClass('authTextBad').text(`Auth Failure: ${JSON.stringify(failedTokens, null, 2)}`)
      } else if (pb.tokens.length === pb.clientConfig.f5List.length) {
        $('#f5AuthText').removeClass('authTextNeutral').addClass('authTextGood').text(`(${pb.clientConfig.f5List.length - remainingTokenCalls.length}/${pb.clientConfig.f5List.length}) Authenticated`)
        cb()
      }
    }
  }
  pb.extendF5AuthToken = function (token) {
    socket.emit('extendF5AuthToken', {
      f5ip: token.ip,
      token: token.token
    })
  }
  pb.handleExtendAuthTokenResponse = function (data) {
    let index = pb.tokens.findIndex(function (t) {
      return t.ip === data.f5ip
    })
    pb.tokens[index] = {
      ip: data.f5ip,
      token: data.token,
      timeout: data.timeout,
      expiration: data.expirationMicros
    }
  }
  pb.autoExtendF5AuthTokens = async function (periodMs, refreshWithin) {
    while (true) {
      await sleep(periodMs)
      pb.tokens.forEach(function (token) {
        let now = new Date()
        let expiration = new Date(token.expiration / 1000)
        let difference = expiration - now
        if (difference < refreshWithin) {
          console.log('Refreshing Token')
          pb.extendF5AuthToken(token)
        }
      })
    }
  }
  
  return pb
}
