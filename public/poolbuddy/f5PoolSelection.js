/* globals $ socket */

var pbF5PoolSelection = function (pb) { // eslint-disable-line no-unused-vars
  // public vars
  pb.selectedPool = ''
  pb.searchUrlPoolStats = ''
  pb.searchUrlPoolMembers = ''
  pb.searchUrlPoolMembersStats = ''
  pb.f5PoolStats = ''
  pb.f5PoolMembers = []
  pb.f5PoolMembersStats = []

  // private vars
  var f5PoolSelectionDiv = `
    <div id="f5PoolSelectionDiv">
      <ul id="f5PoolSelectionUl">
      </ul>
    </div>
  `
  var autoRefreshDiv = `
    <div id="autoRefreshDiv">
      <label class="switch">
        <input type="checkbox" id="autoRefreshCheckbox" name="autoRefreshCheckbox" checked>
        <span class="slider round"></span>
      </label>
      <p>auto-refresh</p>
    </div> 
  `
  var poolStatsDiv = `
    <div id="poolStatsDiv">
      <ul>
        <li><span id="poolStatAvailabilityDot" class="poolStatsNeutral"></span><span id="poolStatsEnabledState"></span></li>
        <li><span id="poolStatsAvailabilityState"></span></li>
        <li><span>ActiveMemberCount: </span><span id="poolStatsActiveMemberCount"></span></li>
      </ul>
    </div>
  `

  // functions
  pb.loadF5PoolSelection = function () {
    pb.searchUrlPoolStats = ''
    pb.searchUrlPoolMembers = ''
    pb.searchUrlPoolMembersStats = ''
    $('#f5PoolSelectionDiv').remove()
    $('#f5SelectionDiv').after(f5PoolSelectionDiv)
    // Check the client Config for the f5 to see if there's any limits under pools
    let configIndex = pb.clientConfig.f5List.findIndex(function (c) {
      return c.ip === pb.selectedF5Ip
    })
    let limitPools = pb.clientConfig.f5List[configIndex].limitPools
    pb.f5Pools.forEach(function (pool) {
      if (limitPools.length === 0 || limitPools.includes(pool.name)) {
        let li = document.createElement('li')
        li.innerHTML = `${pool.name}`
        $(li).data(pool)
        $('#f5PoolSelectionUl').append(li)
      }
    })
    // Adjust height to match
    let height = $('#f5SelectionDiv').height()
    $('#f5PoolSelectionDiv').css('height', height)
    // Add Click function to each li
    $('#f5PoolSelectionUl').find('li').click(function () {
      if ($(this).hasClass('selected')) {
        console.log('Pool already selected')
      } else {
        $(this).addClass('selected').siblings().removeClass('selected')
        // Remove all Sibling Divs after #f5PoolSelectionDiv
        let divCount = $('#f5PoolSelectionDiv').parent().children().length
        let divIndex = $('#f5PoolSelectionDiv').index()
        for (i = divIndex + 2; i < divCount + 1; i++) {
          $('#f5PoolSelectionDiv').parent().children(`:nth-child(${i})`).remove()
        }
        // Make a Div for Auto Refresh Control
        $('#autoRefreshDiv').remove()
        $('#f5PoolSelectionDiv').after(autoRefreshDiv)
        pb.autoRefresh = true
        $('#autoRefreshDiv').find('input').click(function () {
          pb.autoRefresh = !pb.autoRefresh
          $(this).checked = pb.autoRefresh
        })
        // Make a Div for Pool Stats
        $('#poolStatsDiv').remove()
        $('#autoRefreshDiv').after(poolStatsDiv)
        // Call for PoolStats/Members/MembersStats
        pb.selectedPool = `${$(this).data().name}`
        pb.searchUrlPoolStats = `${$(this).data().selfLink.split('?')[0].replace('localhost', pb.selectedF5Ip)}/stats`
        pb.searchUrlPoolMembers = $(this).data().membersReference.link.replace('localhost', pb.selectedF5Ip).split('?')[0]
        pb.searchUrlPoolMembersStats = `${pb.searchUrlPoolMembers}/stats`
        pb.getF5PoolStats()
        pb.getF5PoolMembers()
        pb.getF5PoolMembersStats()
      }
    })
  }

  pb.getF5PoolStats = function () {
    socket.emit('getF5PoolStats', {
      token: pb.activeToken,
      url: pb.searchUrlPoolStats
    })
  }
  pb.getF5PoolMembers = function () {
    socket.emit('getF5PoolMembers', {
      token: pb.activeToken,
      url: pb.searchUrlPoolMembers
    })
  }
  pb.getF5PoolMembersStats = function () {
    socket.emit('getF5PoolMembersStats', {
      token: pb.activeToken,
      url: pb.searchUrlPoolMembersStats
    })
  }

  pb.handleF5PoolStats = function (data) {
    try {
      if (data.url === pb.searchUrlPoolStats) {
        let initF5PoolStats = data.entries
        // Get first property under entries
        initF5PoolStats = initF5PoolStats[Object.keys(initF5PoolStats)[0]]
        initF5PoolStats = initF5PoolStats.nestedStats.entries
        pb.f5PoolStats = {}
        for (let propertyName in initF5PoolStats) {
          let stat = initF5PoolStats[propertyName].value || initF5PoolStats[propertyName].description || ''
          pb.f5PoolStats[propertyName] = stat
        }
        // Update poolStatsDiv
        $('#poolStatsEnabledState').text(` ${pb.f5PoolStats['status.availabilityState']}`)
        if (pb.f5PoolStats['status.availabilityState'] === 'available') {
          $('#poolStatAvailabilityDot').removeClass('poolStatsNeutral poolStatsBad').addClass('poolStatsGood')
        } else {
          $('#poolStatAvailabilityDot').removeClass('poolStatsNeutral poolStatsGood').addClass('poolStatsBad')
        }
        $('#poolStatsActiveMemberCount').text(pb.f5PoolStats['activeMemberCnt'])
      }
    } catch (e) {
      console.log(`Error in pb.handleF5PoolStats: ${e}`)
    }
  }
  pb.handleF5PoolMembers = function (data) {
    let promise = new Promise(async function (resolve, reject) {
      if (data.url === pb.searchUrlPoolMembers) {
        pb.f5PoolMembers = data.items
        resolve(true)
      } else {
        resolve(false)
      }
    })
    return promise
  }
  pb.handleF5PoolMembersStats = async function (data) {
    let promise = new Promise(async function (resolve, reject) {
      if (data.url === pb.searchUrlPoolMembersStats) {
        let initialF5PoolMembersStats = data.entries
        pb.f5PoolMembersStats = []
        for (let propertyName in initialF5PoolMembersStats) {
          let memberStat = initialF5PoolMembersStats[propertyName].nestedStats.entries
          for (let pName in memberStat) {
            memberStat[pName] = memberStat[pName].value || memberStat[pName].description || 0
          }
          pb.f5PoolMembersStats.push(memberStat)
        }
        resolve(true)
      } else { resolve(false) }
    })
    return promise
  }

  return pb
}
