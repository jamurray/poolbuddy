/* globals $ pbF5Auth pbF5Selection pbF5PoolSelection pbF5PoolMembersView pbF5PoolProfileManager */

// ----- poolBuddy Module Form ------------------
var pb = (function () {
  pb = {}
  pb.clientConfig = {}
  pb.autoRefresh = false
  pb.getClientConfig = function () {
    socket.emit('getClientConfig')
  }

  pb.start = function () {
    // augment pb with modules
    pbF5Auth(pb)
    pbF5DeviceSelection(pb)
    pbF5PoolSelection(pb)
    pbF5PoolMembersView(pb)
    pbF5PoolProfileManager(pb)
    pbWindowsControl(pb)

    // Call for config from server
    pb.getClientConfig()

    pb.addAuthDiv() // Start with AuthDiv
    // Continual F5 Stuff
    pb.autoExtendF5AuthTokens(15000, 300000) // Check every 15 seconds, Refresh Tokens Expiring within 5 mins
    pb.continuallyGetF5Data(10000) // Call for new Pool Relevant Data every 10 seconds
    pb.continuallyGetF5States(60000) // Get Active/Standby States for Devices every 60 seconds

    // Continual Windows Stuff
    pb.continuallyPingServers(15000) // ping loop for server ping, 15 seconds
    pb.continuallyGetUpdateStates(5000) // Call every 5 seconds, let server handle duplicate responses
  }

  pb.continuallyGetF5States = async function (ms) {
    while (true) {
      await sleep(ms)
      pb.getF5DeviceStates()
    }
  } 

  pb.continuallyGetF5Data = async function (ms) {
    while (true) {
      await sleep(ms)
      if (pb.searchUrlPoolMembers !== '' && pb.autoRefresh) {
        pb.getF5Nodes()
        pb.getF5NodesStats()
        pb.getF5PoolStats()
        pb.getF5PoolMembersStats()
        pb.getPoolProfiles()
      }
    }
  }

  return pb
})()

// ------- Socket handling ---------------------------------
// --- F5 Auth
socket.on('getClientConfig', function (data) {
  pb.clientConfig = data
})
socket.on('getF5AuthToken', function (data) {
  pb.handleAuthTokenResponse(data, pb.loadF5Selection)
})
socket.on('extendF5AuthToken', function (data) {
  pb.handleExtendAuthTokenResponse(data)
})
socket.on('getF5DeviceStates', function (data) {
  pb.handleF5DeviceStates(data)
})
// --- Events Starting At F5 Selection
socket.on('getF5Nodes', async function (data) {
  await pb.handleF5NodesResponse(data)
  await pb.addF5NodesToMemberViewRowDATA()
})
socket.on('getF5NodesStats', async function (data) {
  await pb.handleF5NodesStatsResponse(data)
  await pb.f5NodesStatsToRowDATA()
  pb.updateNodeStatsColumns()
})
socket.on('getF5Pools', async function (data) {
  await pb.handleF5PoolsResponse(data)
  await pb.loadF5PoolSelection()
})
// ---- Events Starting F5 Pool Selection
socket.on('getF5PoolStats', function (data) {
  pb.handleF5PoolStats(data)
})
socket.on('getF5PoolMembers', async function (data) {
  await pb.handleF5PoolMembers(data)
  await pb.loadF5PoolMembersView()
  await pb.addColumnContextMenu()
  await pb.loadProfileManager()
  pb.addWindowsColumns()
})
socket.on('getF5PoolMembersStats', async function (data) {
  await pb.handleF5PoolMembersStats(data)
  await pb.f5MembersStatsToRowDATA()
  pb.updateMemberStatsColumns()
})
// ---- Pool Profiles
socket.on('getPoolProfiles', async function (data) {
  if (data) {
    pb.handlePoolProfiles(data)
  }
})
// ---- Windows Stuff
socket.on('serverPing', async function (data) {
  if (data) {
    pb.handleServerPing(data)
  }
})
socket.on('rebootServer', async function (data) {
  if (data) {
    pb.handleRebootServer(data)
  }
})
socket.on('restartIIS', async function (data) {
  if (data) {
    pb.handleRestartIIS(data)
  }
})

// ----- Common Stuff -------------------------
function sleep (ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}
socket.on('error', function (err) {
  throw new Error(err)
})
socket.on('message', function (data) {
  console.log(data)
})

