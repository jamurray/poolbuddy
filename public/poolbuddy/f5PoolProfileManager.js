/* globals $ socket */

var pbF5PoolProfileManager = function (pb) { // eslint-disable-line no-unused-vars
  // public vars
  pb.poolProfiles = []
  pb.matchingProfiles = []
  pb.selectedPoolProfile = ''

  // private vars
  var profileManagerDiv = `
    <div id="profileManagerDiv">
      <div id="profileManagerLeft">
        <input type="text" id="profileNameField" maxlength="20" placeholder="poolProfileName">
        <ul>
        </ul>
      </div>
      <div id="profileManagerRight">
        <button id="profileSaveButton">Save</button><!--
        --><button id="profileLoadButton">Load</button><!--
        --><button id="profileDeleteButton">Delete</button>
        <p id=profileManagerMessage></p>
      </div>
    </div>
  `
  // function
  pb.loadProfileManager = function () {
    pb.selectedPoolProfile = ''
    $('#profileManagerDiv').remove()
    $('#poolStatsDiv').after(profileManagerDiv)
    // Button Clicks
    $('#profileSaveButton').click(function () {
      pb.savePoolProfile()
    })
    $('#profileDeleteButton').click(function () {
      pb.deletePoolProfile()
    })
    $('#profileLoadButton').click(function () {
      pb.loadPoolProfile()
    })
  }

  pb.getPoolProfiles = function () {
    socket.emit('getPoolProfiles')
  }

  pb.handlePoolProfiles = function (data) {
    pb.poolProfiles = data.profiles
    if ($('#profileManagerDiv').length === 1) {
      pb.matchingProfiles = pb.poolProfiles.filter(function (profile) {
        return profile.ip === pb.selectedF5Ip &&
        profile.pool === pb.selectedPool
      })
      // If there's matchingProfiles
      if (pb.matchingProfiles.length > 0) {
        pb.matchingProfiles.forEach(function (mp) {
          // Search for exact name in list
          let li = $('#profileManagerDiv').find(`li`)
          let matchingLi = li.filter(function () {
            return $(this).text() === mp.name
          })
          // It doesn't exist, add it
          if (matchingLi.length === 0) {
            $('#profileManagerDiv').find('ul').append(`<li>${mp.name}</li>`)
            // get the new li
            li = $('#profileManagerDiv').find(`li`)
            matchingLi = li.filter(function () {
              return $(this).text() === mp.name
            })[0]
            // add click event
            $(matchingLi).click(function () {
              $('#profileManagerMessage').text('')
              if ($(this).hasClass('selected')) {
                console.log('this pool profile already selected')
              } else {
                $(this).addClass('selected').siblings().removeClass('selected')
                let name = $(this)[0].textContent
                $('#profileNameField').val(name)
                pb.selectedPoolProfile = pb.matchingProfiles.filter(function (p) {
                  return p.name === name
                })[0]
              }
            })
          }
        })
      }
      // Remove Any LI that aren't in matching profiles
      let li = $('#profileManagerDiv').find(`li`)
      for (let i = 0; i < li.length; i++) {
        let name = $(li[i]).text()
        let index = pb.matchingProfiles.findIndex(function (p) {
          return p.name === name
        })
        if (index === -1) {
          $(li[i]).remove()
        }
      }
    }
  }

  pb.savePoolProfile = async function () {
    let profileName = $('#profileNameField').val()
    let profileNameGood = false
    // Determine if the name is good
    $('#profileManagerMessage').text('')
    if (profileName.length === 0) {
      console.log('save: pool profile name cannot be empty')
      $('#profileManagerMessage').text('save: name cannot be empty')
    } else if (profileName.length > 20) {
      console.log('pool profile name cannot be longer than 20')
      $('#profileManagerMessage').text('name longer than 20')
    } else {
      profileNameGood = true
    }

    if (profileNameGood) {
      $('#autoRefreshCheckbox')[0].checked = false
      await sleep(2000) // Give it a couple seconds in case any open calls comes in
      let profile = {
        name: profileName,
        ip: pb.selectedF5Ip,
        pool: pb.selectedPool,
        states: []
      }
      let rows = $('#f5PoolMembersDiv').find('.row')
      for (let i = 0; i < rows.length; i++) {
        let row = $(rows[i])
        let ip = row.data().address
        // Member State
        let mstate = ''
        if (row.data().MEMBERSTATS['status.statusReason'] === 'Forced down' && row.data().MEMBERSTATS['sessionStatus'] === 'user-disabled') {
          mstate = 'forced'
        } else if (row.data().MEMBERSTATS['status.enabledState'] === 'disabled-by-parent') {
          mstate = 'enabled'
        } else if (row.data().MEMBERSTATS['status.enabledState'] === 'disabled' && row.data().MEMBERSTATS['sessionStatus'] === 'user-disabled') {
          mstate = 'disabled'
        } else if (row.data().MEMBERSTATS['status.enabledState'] === 'enabled') {
          mstate = 'enabled'
        } else {
          console.error('pb.savePoolProfile did not determine member state for row')
          console.log(row.data())
        }
        // Parent Node State
        let nstate = ''
        if (row.data().NODESTATS['status.statusReason'] === 'Forced down' && row.data().NODESTATS['sessionStatus'] === 'user-disabled') {
          nstate = 'forced'
        } else if (row.data().NODESTATS['status.enabledState'] === 'disabled' && row.data().NODESTATS['sessionStatus'] === 'user-disabled') {
          nstate = 'disabled'
        } else if (row.data().NODESTATS['status.enabledState'] === 'enabled') {
          nstate = 'enabled'
        } else {
          console.error('pb.savePoolProfile did not determine parent node state for row')
          console.log(row.data())
        }
        profile.states.push({
          'ip': ip,
          'mstate': mstate,
          'nstate': nstate
        })
        $(row).removeClass('saveOnRow')
      }
      console.log(profile)
      $('#autoRefreshCheckbox')[0].checked = true
      socket.emit('savePoolProfile', profile)
    }
  }

  pb.loadPoolProfile = async function () {
    $('#profileManagerMessage').text('')
    if (pb.selectedPoolProfile !== '') {
      console.log(`loading profile: ${pb.selectedPoolProfile.name}`)
      $('#profileManagerMessage').text(`loading profile: ${pb.selectedPoolProfile.name}`)
      console.log(pb.selectedPoolProfile)

      let rows = $('#f5PoolMembersDiv').find('.row')
      for (let i = 0; i < pb.selectedPoolProfile.states.length; i++) {
        let state = pb.selectedPoolProfile.states[i]
        // console.log(state)
        // Find row with matching ip
        let matchingRow = $(rows.filter(function () {
          return $(this).data().address === state.ip
        })[0])
        matchingRow.addClass('loadOnRow')

        // Get current member state
        let currentMstate = ''
        if (matchingRow.data().MEMBERSTATS['status.statusReason'] === 'Forced down' &&
            matchingRow.data().MEMBERSTATS['sessionStatus'] === 'user-disabled') {
          currentMstate = 'forced'
        } else if (matchingRow.data().MEMBERSTATS['status.enabledState'] === 'disabled-by-parent') {
          currentMstate = 'enabled'
        } else if (matchingRow.data().MEMBERSTATS['status.enabledState'] === 'disabled' &&
            matchingRow.data().MEMBERSTATS['sessionStatus'] === 'user-disabled') {
          currentMstate = 'disabled'
        } else if (matchingRow.data().MEMBERSTATS['status.enabledState'] === 'enabled') {
          currentMstate = 'enabled'
        }
        // Compare current Member state to desired Member state
        if (currentMstate !== state.mstate) {
          // change Member state
          if (state.mstate === 'enabled') {
            console.log(`enabling member: ${state.ip}`)
            matchingRow.find('.stateButton.enable.member').click()
          } else if (state.mstate === 'disabled') {
            console.log(`disabling member: ${state.ip}`)
            matchingRow.find('.stateButton.disable.member').click()
          } else if (state.mstate === 'forced') {
            console.log(`forcing offline member: ${state.ip}`)
            matchingRow.find('.stateButton.force.member').click()
          }
        }

        // Get current Parent Node state
        let currentNstate = ''
        if (matchingRow.data().NODESTATS['status.statusReason'] === 'Forced down' &&
            matchingRow.data().NODESTATS['sessionStatus'] === 'user-disabled') {
          currentNstate = 'forced'
        } else if (matchingRow.data().NODESTATS['status.enabledState'] === 'disabled' &&
            matchingRow.data().NODESTATS['sessionStatus'] === 'user-disabled') {
          currentNstate = 'disabled'
        } else if (matchingRow.data().NODESTATS['status.enabledState'] === 'enabled') {
          currentNstate = 'enabled'
        }
        // Compare current Parent Node state to desired Parent Node state
        if (currentNstate !== state.nstate) {
          // change parent node state
          if (state.nstate === 'enabled') {
            console.log(`enabling node: ${state.ip}`)
            matchingRow.find('.stateButton.enable.node').click()
          } else if (state.nstate === 'disabled') {
            console.log(`disabling node: ${state.ip}`)
            matchingRow.find('.stateButton.disable.node').click()
          } else if (state.nstate === 'forced') {
            console.log(`forcing offline node: ${state.ip}`)
            matchingRow.find('.stateButton.force.node').click()
          }
        }
        await sleep(20)
        matchingRow.removeClass('loadOnRow')
      }
      $('#profileManagerMessage').text('')
    }
  }

  pb.deletePoolProfile = function () {
    $('#profileManagerMessage').text('')
    if (pb.selectedPoolProfile !== '') {
      socket.emit('deletePoolProfile', pb.selectedPoolProfile)
    }
  }

  return pb
}
