// Powershell functions I can load inline into socket functions calling Octopus

var pfuncGetWIAStatusValue = `
function Get-WIAStatusValue($value) { 
  switch -exact ($value) { 
      0 {"NotStarted"}
      1 {"InProgress"}
      2 {"Succeeded"}
      3 {"SucceededWithErrors"}
      4 {"Failed"} 
      5 {"Aborted"} 
  } 
}
`

var pfuncGetPendingUpdates = `
function Get-PendingUpdates {
  $session = New-Object -ComObject Microsoft.Update.Session
  $searcher = $session.CreateUpdateSearcher()
  $result = $searcher.Search("IsInstalled=0" )
  return $result.Updates
}
`

var pfuncGetInstalledUpdates = `
function Get-InstalledUpdates {
  $session = New-Object -ComObject Microsoft.Update.Session
  $searcher = $session.CreateUpdateSearcher()
  $result = $searcher.Search("IsInstalled=1" )
  $result.Updates | select Title, LastDeploymentChangeTime, RebootRequired
}
`

var pfuncDownloadPendingUpdates = ` 
function Download-PendingUpdates {
  $pendingUpdates = Get-PendingUpdates
  $updatesNeedingDownload = $pendingUpdates | Where-Object {$_.IsDownloaded -eq $false}
  if ($pendingUpdates.Count -eq 0) {
      Write-Host "No pending updates found"
  } ElseIf ($updatesNeedingDownload.Count -eq 0) {
      Write-Host "No pending updates need to be download"
  } ElseIf ($updatesNeedingDownload.Count -gt 0) {
      $updatesCollection = New-Object -ComObject Microsoft.Update.UpdateColl
      foreach ($update in $updatesNeedingDownload) {
          $updatesCollection.add($update) | Out-Null
      }
      $session = New-Object -ComObject Microsoft.Update.Session
      $downloader = $session.CreateUpdateDownloader()
      $downloader.Updates = $updatesCollection
      $downloadResult = $downloader.Download()
      $resultMessage = "Pending Updates Download - $(Get-WIAStatusValue($downloadResult.ResultCode))"
      Write-Host $resultMessage
  }
}
`

var pfuncInstallPendingUpdates = `
function Install-PendingUpdates {
  $pendingUpdates = Get-PendingUpdates
  if ($pendingUpdates.Count -eq 0) {
      Write-Host "No pending updates found"
  } ElseIf ($pendingUpdates.Count -gt 0) {
      $updatesCollection = New-Object -ComObject Microsoft.Update.UpdateColl
      foreach ($update in $pendingUpdates) {
          $updatesCollection.add($update) | Out-Null
      }
      $session = New-Object -ComObject Microsoft.Update.Session
      $installer = $session.CreateUpdateInstaller()
      $installer.Updates = $updatesCollection
      $installResult = $installer.install()
      $resultMessage = "Pending Updates Install - $(Get-WIAStatusValue($installResult.ResultCode))"
      Write-Host $resultMessage
  }
}
`

var pfuncGetPendingReboot = `
function Get-PendingReboot {
  #Note that these reg keys are automatically deleted when the machine reboots as it's volatile (only held in memory).
  if (Get-ChildItem "HKLM:/Software/Microsoft/Windows/CurrentVersion/Component Based Servicing/RebootPending" -ErrorAction Ignore) { 
      return $true 
  }
  if (Get-Item "HKLM:/SOFTWARE/Microsoft/Windows/CurrentVersion/WindowsUpdate/Auto Update/RebootRequired" -ErrorAction Ignore) {
      return $true 
  }
  try { 
      $util = [wmiclass]"//./root/ccm/clientsdk:CCM_ClientUtilities"
      $status = $util.DetermineIfRebootPending()
      if(($status -ne $null) -and $status.RebootPending){
          return $true
      }
  } catch {}
  return $false
}
`

var pfuncGetUpdateStatus = `
function Get-UpdateStatus {
  # Check if Reboot Required
  $pendingReboot = Get-PendingReboot
  if ($pendingReboot) {
      return "rebootPending"
  }
  # Check if an update is installing
  $runningInstallers =  Get-Process | Where-Object {
      $_.ProcessName -like "*msiexec*" -or
      $_.ProcessName -like "*setup*"
  }
  # Check for Pending Updates
  if ($runningInstallers.Count -gt 0) {
      return "installing"
  }
  $pendingUpdates = Get-PendingUpdates
  if ($pendingUpdates.Count -gt 0) {
      return "pendingUpdatesCount: $($pendingUpdates.Count)"
  }
  return "updated"
}
`

module.exports = {
  pfuncGetWIAStatusValue: pfuncGetWIAStatusValue,
  pfuncGetPendingUpdates: pfuncGetPendingUpdates,
  pfuncGetInstalledUpdates: pfuncGetInstalledUpdates,
  pfuncDownloadPendingUpdates: pfuncDownloadPendingUpdates,
  pfuncInstallPendingUpdates: pfuncInstallPendingUpdates,
  pfuncGetPendingReboot: pfuncGetPendingReboot,
  pfuncGetUpdateStatus: pfuncGetUpdateStatus
}