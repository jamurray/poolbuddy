const colors = require('colors')
const fs = require('fs')
const path = require('path')

// Create Server
const express = require('express')
const app = express()

// https version
const server = require('https').createServer({
  key: fs.readFileSync(path.join(__dirname, '../cert/key.pem')),
  cert: fs.readFileSync(path.join(__dirname, '../cert/certificate.pem'))
}, app)
// http version
// var server = require('http').createServer(app)

// Create IO listener for server and augment it with handlers
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0 // Ignore F5 Self Signed Cert Errors
const io = require('socket.io')(server)
const initial = require('./handlers/handler_initial.js')
initial.initialHandling(io)
const f5Auth = require('./handlers/handler_f5Auth.js')
f5Auth.authHandling(io)
const f5DeviceSelection = require('./handlers/handler_f5DeviceSelection.js')
f5DeviceSelection.deviceSelectionHandling(io)
const f5PoolSelection = require('./handlers/handler_f5PoolSelection.js')
f5PoolSelection.poolSelectionHandling(io)
const f5MemberNodeControl = require('./handlers/handler_f5MemberNodeControl.js')
f5MemberNodeControl.controlHandling(io)
const poolProfile = require('./handlers/handler_poolProfile.js')
poolProfile.profileHandling(io)
const windows = require('./handlers/handler_windows.js')
windows.windowsHandling(io)



// Server everything in public at /
app.use('/', express.static(path.join(__dirname, '../public')))

// Start the server
const port = 9008
server.listen(port, function () {
  // console.log('toolbox server started. listening on port %d in %s mode', port, app.get('env'))
  console.log(colors.bgCyan.black(`serving at http://localhost:${port}`))
})
