// This can reach out to some servers and make calls
// through Octopus API to run powershell on servers
const ping = require('ping')
const fetch = require('node-fetch')
const moment = require('moment')
const {config} = require('../windowsConfig.json')
var apiKey = config.octopusApiKey
var octopusUrl = config.octopusUrl

module.exports = {
  windowsHandling: windowsHandling
}

function windowsHandling (io) {
  io.on('connection', function (socket) {
    socket.on('serverPing', function (data) {
      serverPing(socket, data)
    })
    // socket.on('GetUpdateStatesForServers', function (data) {
    //   getUpdateStatesForServers(socket, data)
    // })
    socket.on('rebootServer', function (data) {
      rebootServer(socket, data)
    })
    socket.on('restartIIS', function (data) {
      restartIIS(socket, data)
    })
  })
}
// ------------------------------------------------------
// Ping
function serverPing(socket, data) {
  ping.promise.probe(data.host, {
    timeout: 1
  })
  .then(res => {
    res.requestedHost = data.host
    socket.emit('serverPing', res)
  })
  .catch(err => console.log(err))
}
// Sleep
function sleep (ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

// Get's the full machine list known by octopus into memory
var allMachinesOcto = []
async function getAllMachinesOcto () {
  return new Promise(function (resolve, reject) {
    let headers = {'X-Octopus-ApiKey': apiKey}
    fetch(`${octopusUrl}/api/machines/all/`, {
      method: 'GET',
      headers: headers
    })
    .then(response => response.json())
    .then(data => {
      allMachinesOcto = data
      resolve()
    })
  })
}

// Continually update list of machines known by Octo
async function continuallyUpdateAllMachinesOcto () {
  await getAllMachinesOcto()
  while (true) {
    await sleep(1 * 60 * 1000) // 5 minutes
    await getAllMachinesOcto()
  }
}
continuallyUpdateAllMachinesOcto(allMachinesOcto)

async function getRelatedOctoMachines(serverNameArray) {
  let relatedOctoMachines = []
  // serverNames.forEach(function (name) {
  for (let i = 0; i < serverNameArray.length; i++) {
    let name = serverNameArray[i]
    // console.log(name)
    // console.log(allMachinesOcto)
    let index = allMachinesOcto.findIndex(function (m) {
      return m.Name.toUpperCase() === name.toUpperCase()
    })
    // console.log(index)
    if (index > -1) {
      relatedOctoMachines.push(allMachinesOcto[index])
    }
  }
  return relatedOctoMachines
}

// ----------- UPDATE STATES FOR MACHINES THROUGH OCTOPUS TASKS ---------------------
var updateStateTasks = []

async function cleanOldUpdateStateTasks () {
  while (true) {
    await sleep(5000)
    // console.log('cleaner')
    // find any task with a startTime older than 60 seconds and delete it
    let currentTime = moment().valueOf()
    updateStateTasks.forEach(function (task) {
      console.log(currentTime)
      console.log(task.startTime)
      let timeDifference = currentTime - task.startTime
      console.log(`timeDifference: ${timeDifference}`)
      if (timeDifference > 60000) {
        console.log('delete task')
      }
    })
  }
}
// cleanOldUpdateStateTasks()

async function getUpdateStatesForServers(socket, data) {
  // determine if there is a task for this list already
  let index = updateStateTasks.findIndex(function (task) {
    return compareOrderedArrays(task.serverNames, data.serverNames)
  })
  if (index > -1) { // It is already in the array
    console.log(updateStateTasks)
    console.log('this is already in updateStateTasks Array')
    // Check for results
  } else {  // It is not in the Array
    data.startTime = moment().valueOf()
    updateStateTasks.push(data)
  }
}

function compareOrderedArrays (array1, array2) {
  // Compare Length first
  if (array1.length !== array2.length) {
    return false
  }
  // Compare Contents of arrays in order
  let same = true
  for (let i = 0; i < array1.length; i++) {
    if (array1[i] !== array2[i]) {
      same = false
    }
  }
  return same
}






// ---------------------- Reboot Server -----------------------
async function rebootServer(socket, data) {
  let serverName = data.serverName
  // Verify Authorization
  let authorized = (config.authorizedUsers.includes(socket.username))
  if (authorized) {
    // match the server from all machines
    let index = -1
    try {
      index = allMachinesOcto.findIndex(function (m) {
        return m.Name.toUpperCase() === serverName.toUpperCase()
      })
    } catch (e) {
      console.error(e)
      console.log(allMachinesOcto)
    }
    if (index > -1) {
      let machine = allMachinesOcto[index]
      // console.log(machine.Id)
      // Create an Octopus Reboot Task for this server
      let headers = {'X-Octopus-ApiKey': apiKey}
      let body = {
        "Name": `AdHocScript`,
        "Description": `reboot server: ${serverName}`,
        "Arguments": {
          "MachineIds" : [machine.Id],
          "TenantIds" : [],
          "TargetRoles": [],
          "EnvironmentIds": [],
          "WorkerIds": [],
          "Syntax": "Powershell",
          "ScriptBody": "Restart-Computer"
        },
        "SpaceId": machine.SpaceId
      }
      fetch(`${octopusUrl}/api/tasks`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(body)
      })
      .then(response => response.json())
      .then(octoResponse => { 
        socket.emit('rebootServer', {
          success: true,
          serverName: serverName,
          octopusUrl: octopusUrl,
          octoResponse: octoResponse
        })
      })
    } else { // Machine not found by name
      socket.emit('rebootServer', {
        success: false,
        reason: 'machine not found in Octopus',
        serverName: serverName
      })
    }
  } else { // Not Authorized
    socket.emit('rebootServer', {
      success: false,
      reason: 'not authorized to perform windows actions',
      serverName: serverName
    })
  }
}

async function restartIIS(socket, data) {
  let serverName = data.serverName
  // Verify Authorization
  let authorized = (config.authorizedUsers.includes(socket.username))
  if (authorized) {    
    // match the server from all machines
    let index = -1
    try {
      index = allMachinesOcto.findIndex(function (m) {
        return m.Name.toUpperCase() === serverName.toUpperCase()
      })
    } catch (e) {
      console.error(e)
      console.log(allMachinesOcto)
    }
    if (index > -1) {
      let machine = allMachinesOcto[index]
      // console.log(machine.Id)
      // Create an Octopus Reboot Task for this server
      let headers = {'X-Octopus-ApiKey': apiKey}
      let body = {
        "Name": `AdHocScript`,
        "Description": `restart IIS: ${serverName}`,
        "Arguments": {
          "MachineIds" : [machine.Id],
          "TenantIds" : [],
          "TargetRoles": [],
          "EnvironmentIds": [],
          "WorkerIds": [],
          "Syntax": "Powershell",
          "ScriptBody": "Get-Service w3svc | Restart-Service -Verbose -PassThru"
        },
        "SpaceId": machine.SpaceId
      }
      fetch(`${octopusUrl}/api/tasks`, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(body)
      })
      .then(response => response.json())
      .then(octoResponse => { 
        socket.emit('restartIIS', {
          success: true,
          serverName: serverName,
          octopusUrl: octopusUrl,
          octoResponse: octoResponse
        })
      })
    } else { // Machine not found by name
      socket.emit('restartIIS', {
        success: false,
        reason: 'machine not found in Octopus',
        serverName: serverName
      })
    }
  } else { // Not Authorized
    socket.emit('restartIIS', {
      success: false,
      reason: 'not authorized to perform windows actions',
      serverName: serverName
    })
  }

  
}





async function test() {
  await sleep(3000)
  relatedOctoMachines = await getRelatedOctoMachines(['SVD0APIS26', 'SVD0APIS25', 'SVD0DMZDAPID01'])
  console.log(relatedOctoMachines)
}
// test()

