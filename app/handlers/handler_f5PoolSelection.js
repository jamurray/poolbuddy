const ipToFqdn = require('../ipToFqdn.json')
const fetch = require('node-fetch')

module.exports = {
  poolSelectionHandling: poolSelectionHandling
}

function poolSelectionHandling (io) {
  io.on('connection', function (socket) {
    socket.on('getF5PoolStats', function (data) {
      getUrlWithToken(socket, 'getF5PoolStats', data.token, data.url)
    })
    socket.on('getF5PoolMembers', function (data) {
      getF5PoolMembers(socket, data.token, data.url)
    })
    socket.on('getF5PoolMembersStats', function (data) {
      getUrlWithToken(socket, 'getF5PoolMembersStats', data.token, data.url)
    })
  })
}

function getUrlWithToken (socket, callName, token, url) {
  let headers = {
    'Content-Type': 'application/json',
    'X-F5-Auth-Token': token
  }
  fetch(url, {
    method: 'GET',
    headers: headers
  })
  .then(response => response.json())
  .then(data => {
    data.url = url
    socket.emit(callName, data)
  })
  .catch(err => console.log(err))
}

function getF5PoolMembers (socket, token, url) {
  let headers = {
    'Content-Type': 'application/json',
    'X-F5-Auth-Token': token
  }
  fetch(url, {
    method: 'GET',
    headers: headers
  })
  .then(response => response.json())
  .then(async data => {
    data = await addNamesToPoolMemberResults(data)
    data.url = url
    socket.emit('getF5PoolMembers', data)
  })
  .catch(err => console.log(err))
}

async function addNamesToPoolMemberResults (data) {
  // Search my local file for fqdn
  let nodes = data.items
  if (typeof nodes.length !== 'undefined') {
    for (let i = 0; i < nodes.length; i++) {
      let node = nodes[i]
      // console.log(colors.yellow(node.address))
      let recordIndex = ipToFqdn.findIndex(function (record) {
        return record.ip === node.address
      })
      let fqdn = ''
      let hostname = ''
      let suffixNumber = ''
      if (recordIndex === -1) {
        fqdn = ''
        hostname = ''
        suffixNumber = ''
      } else {
        fqdn = fqdn = ipToFqdn[recordIndex].fqdn
        hostname = fqdn.split('.')[0]
        suffixNumber = parseInt(hostname.match(/\d+$/))
      }
      // console.log(colors.cyan(fqdn))
      node.FQDN = fqdn
      node.HOSTNAME = hostname
      node.SUFFIXNUMBER = suffixNumber
    }
    // Reorder by Hostname Suffix Number
    nodes = nodes.sort(sortBySuffixNumber)
  }
  return data
}

function sortBySuffixNumber (a, b) {
  let aValue = a.SUFFIXNUMBER || 9999
  let bValue = b.SUFFIXNUMBER || 9999
  if (aValue < bValue) { return -1 }
  if (aValue > bValue) { return 1 }
  return 0
}
