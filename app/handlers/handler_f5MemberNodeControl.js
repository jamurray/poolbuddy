const fetch = require('node-fetch')

module.exports = {
  controlHandling: controlHandling
}

function controlHandling (io) {
  io.on('connection', function (socket) {
    socket.on('changeStateF5PoolMember', function (data) {
      changeStateF5PoolMemberOrNode(data.token, data.url, data.state)
    })
    socket.on('changeStateF5Node', function (data) {
      changeStateF5PoolMemberOrNode(data.token, data.url, data.state)
    })
  })
}

function changeStateF5PoolMemberOrNode (token, url, state) {
  let headers = {
    'Content-Type': 'application/json',
    'X-F5-Auth-Token': token
  }
  let body
  if (state === 'enable') { body = { 'session': 'user-enabled', 'state': 'user-up' }
  } else if (state === 'disable') { body = { 'session': 'user-disabled', 'state': 'user-up' }
  } else if (state === 'force') { body = { 'session': 'user-disabled', 'state': 'user-down' } }
  fetch(url, {
    method: 'PATCH',
    headers: headers,
    body: JSON.stringify(body)
  })
}
