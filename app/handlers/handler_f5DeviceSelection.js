const fetch = require('node-fetch')

module.exports = {
  deviceSelectionHandling: deviceSelectionHandling
}

function deviceSelectionHandling (io) {
  io.on('connection', function (socket) {
    socket.on('getF5Pools', function (data) {
      getUrlWithToken(socket, 'getF5Pools', data.token, data.url)
    })
    socket.on('getF5Nodes', function (data) {
      getUrlWithToken(socket, 'getF5Nodes', data.token, data.url)
    })
    socket.on('getF5NodesStats', function (data) {
      getUrlWithToken(socket, 'getF5NodesStats', data.token, data.url)
    })
    socket.on('getF5DeviceStates', function (data) {
      getUrlWithToken(socket, 'getF5DeviceStates', data.token, data.url)
    })
  })
}

function getUrlWithToken (socket, callName, token, url) {
  let headers = {
    'Content-Type': 'application/json',
    'X-F5-Auth-Token': token
  }
  fetch(url, {
    method: 'GET',
    headers: headers
  })
  .then(response => response.json())
  .then(data => {
    data.url = url
    socket.emit(callName, data)
  })
  .catch(err => console.log(err))
}
