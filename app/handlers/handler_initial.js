const clientConfig = require('../clientConfig.json')

module.exports = {
  initialHandling: initialHandling
}

// --------- Initial Handling -----------------
function initialHandling (io) {
  io.on('connection', function (socket) {
    socket.emit('message', 'socketIO connection with server established')

    socket.on('message', function (data) {
      console.log(data)
      socket.emit('message', 'message recieved')
    })

    socket.on('getClientConfig', function (data) {
      socket.emit('getClientConfig', clientConfig)
    })
  })
}