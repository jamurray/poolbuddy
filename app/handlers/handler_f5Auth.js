const fetch = require('node-fetch')

module.exports = {
  authHandling: authHandling
}

// ---- Auth Calls ------------------
function authHandling (io) {
  io.on('connection', function (socket) {
    socket.on('getF5AuthToken', function (data) {
      getF5AuthToken(socket, data.f5ip, data.username, data.password)
    })
    socket.on('extendF5AuthToken', function (data) {
      extendF5AuthToken(socket, data.f5ip, data.token)
    })
  })
}

function getF5AuthToken (socket, f5ip, username, password) {
  let body =  {
            'username': username,
            'password': password,
            'loginProviderName': 'tmos'
          }
  fetch(`https://${f5ip}/mgmt/shared/authn/login`, {
    method: 'POST',
    body: JSON.stringify(body)
  })
  .then(response => response.json())
  .then(data => {
    // If it is valid, add the username to the socket
    // This can be used later to verify account for windows operations
    // Example: an instance where someone should be able to auth to F5, but not perform windows operations
    if (data.token) {
      socket.username = username
    }
    data.f5ip = f5ip
    socket.emit('getF5AuthToken', data)
  })
  .catch(err => console.log(err))
}

function extendF5AuthToken (socket, f5ip, token) {
  let headers = {
    'Content-Type': 'application/json',
    'X-F5-Auth-Token': token
  }
  let body = { 'timeout': '36000' }
  fetch(`https://${f5ip}/mgmt/shared/authz/tokens/${token}`, {
    method: 'PATCH',
    headers: headers,
    body: JSON.stringify(body),
  })
  .then(response => response.json())
  .then(data => { 
    data.f5ip = f5ip
    socket.emit('extendF5AuthToken', data)
   })
   .catch(err => console.log(err))
}
