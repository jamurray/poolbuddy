const fs = require('fs')
const path = require('path')
const poolProfileJsonPath = path.join(__dirname, '../poolProfiles.json')

module.exports = {
  profileHandling: profileHandling
}

function profileHandling (io) {
  io.on('connection', function (socket) {
    socket.on('getPoolProfiles', function () {
      getPoolProfiles()
        .then(function (response) {
          socket.emit('getPoolProfiles', response)
        }, function (err) {
          return err
        })
    })
    socket.on('savePoolProfile', function (data) {
      savePoolProfile(data)
        .then(function (response) {
          socket.emit('getPoolProfiles', response)
        }, function (err) {
          return err
        })
    })
    socket.on('deletePoolProfile', function (data) {
      deletePoolProfile(data)
        .then(function (response) {
          socket.emit('deletePoolProfile', response)
        }, function (err) {
          return err
        })
    })
  })
}

function getPoolProfiles () {
  let promise = new Promise(async function (resolve, reject) {
    fs.readFile(poolProfileJsonPath, { encoding: 'utf-8' }, function (err, data) {
      if (!err) {
        // console.log(data)
        let profiles = JSON.parse(data)
        // console.log(profiles)
        resolve(profiles)
      } else {
        console.log(err)
        reject(err)
      }
    })
  })
  return promise
}

async function savePoolProfile (profileToSave) {
  let profilesJsonFile = await getPoolProfiles()
  let profiles = profilesJsonFile.profiles
  // console.log(profiles)
  // Search for a match of ip+poolname+profileName
  let index = profiles.findIndex(function (profile) {
    return profileToSave.ip === profile.ip &&
    profileToSave.pool === profile.pool &&
    profileToSave.name === profile.name
  })
  if (index === -1) { // Not found, new profile
    // console.log('new Profile')
    profiles.push(profileToSave)
  } else { // Existing profile
    // console.log('existing Profile')
    profiles[index] = profileToSave
  }
  let json = JSON.stringify({ 'profiles': profiles }, null, 2)
  fs.writeFile(poolProfileJsonPath, json, 'utf8', function () {
    // console.log('saved')
  })
}

async function deletePoolProfile (profileToDelete) {
  let profilesJsonFile = await getPoolProfiles()
  let profiles = profilesJsonFile.profiles
  let index = profiles.findIndex(function (profile) {
    return profileToDelete.ip === profile.ip &&
    profileToDelete.pool === profile.pool &&
    profileToDelete.name === profile.name
  })
  // console.log(index)
  if (index !== -1) {
    profiles.splice(index, 1)
  }
  let json = JSON.stringify({ 'profiles': profiles }, null, 2)
  fs.writeFile(poolProfileJsonPath, json, 'utf8', function () {
    // console.log('saved')
  })
}
